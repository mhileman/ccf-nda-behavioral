Cognition Crystallized Composite v1.1
Cognition Early Childhood Composite v1.1
Cognition Fluid Composite v1.1
Cognition Total Composite Score v1.1
Negative Affect Summary (13-17)
Negative Affect Summary (18+)
Negative Affect Summary (8-12)
Negative Social Perception Summary (13-17)
Negative Social Perception Summary (8-12)
NIH Toolbox 2-Minute Walk Endurance Test Age 3+ v2.0
NIH Toolbox 4-Meter Walk Gait Speed Test Age 7+ v2.0
NIH Toolbox 9-Hole Pegboard Dexterity Test Age 3+ v2.0
NIH Toolbox Anger FF Ages 8-17 v2.0
NIH Toolbox Anger-Affect CAT Age 18+ v2.0
NIH Toolbox Anger-Hostility FF Age 18+ v2.0
NIH Toolbox Anger-Physical Aggression FF Age 18+ v2.0
NIH Toolbox Dimensional Change Card Sort Test Age 12+ v2.1
NIH Toolbox Dimensional Change Card Sort Test Ages 3-7 v2.1
NIH Toolbox Dimensional Change Card Sort Test Ages 8-11 v2.1
NIH Toolbox Emotional Support FF Age 18+ v2.0
NIH Toolbox Emotional Support FF Ages 8-17 v2.0
NIH Toolbox Fear FF Ages 8-17 v2.0
NIH Toolbox Fear-Affect CAT Age 18+ v2.0
NIH Toolbox Fear-Somatic Arousal FF Age 18+ v2.0
NIH Toolbox Flanker Inhibitory Control and Attention Test Age 12+ v2.1
NIH Toolbox Flanker Inhibitory Control and Attention Test Ages 3-7 v2.1
NIH Toolbox Flanker Inhibitory Control and Attention Test Ages 8-11 v2.1
NIH Toolbox Friendship FF Age 18+ v2.0
NIH Toolbox Friendship FF Ages 8-17 v2.0
NIH Toolbox General Life Satisfaction CAT Age 18+ v2.0
NIH Toolbox General Life Satisfaction CAT Ages 13-17 v2.0
NIH Toolbox General Life Satisfaction FF Ages 8-12 v2.0
NIH Toolbox Grip Strength Test Age 3+ v2.0
NIH Toolbox Instrumental Support FF Age 18+ v2.0
NIH Toolbox List Sorting Working Memory Test Age 7+ v2.1
NIH Toolbox List Sorting Working Memory Test Ages 3-6 v2.1
NIH Toolbox Loneliness FF Age 18+ v2.0
NIH Toolbox Loneliness FF Ages 8-17 v2.0
NIH Toolbox Meaning and Purpose CAT Age 18+ v2.0
NIH Toolbox Odor Identification Test Age 10+ v2.0
NIH Toolbox Odor Identification Test Ages 3-9 v2.0
NIH Toolbox Oral Reading Recognition Test Age 3+ v2.0
NIH Toolbox Pain Interference CAT Age 18+ v2.0
NIH Toolbox Pattern Comparison Processing Speed Test Age 7+ v2.1
NIH Toolbox Pattern Comparison Processing Speed Test Ages 3 - 6 v2.1
NIH Toolbox Perceived Hostility FF Age 18+ v2.0
NIH Toolbox Perceived Hostility FF Ages 8-17 v2.0
NIH Toolbox Perceived Rejection FF Age 18+ v2.0
NIH Toolbox Perceived Rejection FF Ages 8-17 v2.0
NIH Toolbox Perceived Stress FF Age 18+ v2.0
NIH Toolbox Perceived Stress FF Ages 13-17 v2.0
NIH Toolbox Picture Sequence Memory Test Age 7 Form A v2.1
NIH Toolbox Picture Sequence Memory Test Age 8+ Form A v2.1
NIH Toolbox Picture Sequence Memory Test Ages 5-6 Form A v2.1
NIH Toolbox Picture Vocabulary Test Age 3+ v2.0
NIH Toolbox Positive Affect CAT Age 18+ v2.0
NIH Toolbox Positive Affect CAT Ages 13-17 v2.0
NIH Toolbox Positive Affect FF Ages 8-12 v2.0
NIH Toolbox Sadness CAT Age 18+ v2.0
NIH Toolbox Sadness FF Ages 8-17 v2.0
NIH Toolbox Self-Efficacy CAT Age 18+ v2.0
NIH Toolbox Self-Efficacy CAT Ages 13-17 v2.0
NIH Toolbox Self-Efficacy CAT Ages 8-12 v2.0
NIH Toolbox Visual Acuity Practice Age 8+ v2.0
NIH Toolbox Visual Acuity Practice Ages 3-7 v2.0
NIH Toolbox Visual Acuity Test Age 8+ v2.0
NIH Toolbox Visual Acuity Test Ages 3-7 v2.0
NIH Toolbox Words-In-Noise Test Age 6+ v2.1
Psychological Well Being Summary (18+)
Social Satisfaction Summary (13-17)
Social Satisfaction Summary (18+)
Social Satisfaction Summary (8-12)
